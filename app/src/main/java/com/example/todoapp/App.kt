package com.example.todoapp

import android.app.Application
import dagger.hilt.android.HiltAndroidApp

/**
 * Created by Muhammad AKKAD on 10/11/21.
 */
@HiltAndroidApp
class App : Application() {

    override fun onCreate() {
        super.onCreate()
    }
}