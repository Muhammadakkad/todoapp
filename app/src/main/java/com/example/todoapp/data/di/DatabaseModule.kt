package com.example.todoapp.data.di

import android.content.Context
import androidx.room.Room
import com.example.todoapp.data.Constant
import com.example.todoapp.data.db.AppDataBase
import com.example.todoapp.data.db.ToDoDao
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.qualifiers.ApplicationContext
import dagger.hilt.components.SingletonComponent

/**
 * Created by Muhammad AKKAD on 10/25/21.
 */
@InstallIn(SingletonComponent::class)
@Module
object DatabaseModule {

    @Provides
    fun provideDatabase(@ApplicationContext appContext: Context): AppDataBase {
        return Room.databaseBuilder(
            appContext,
            AppDataBase::class.java,
            Constant.TODO_DATABASE
        ).build()
    }

    @Provides
    fun provideTodoDao(database: AppDataBase): ToDoDao {
        return database.toDoDao()
    }


}