package com.example.todoapp.data.db

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import com.example.todoapp.data.Constant
import com.example.todoapp.data.model.ToDoModel

/**
 * Created by Muhammad AKKAD on 10/25/21.
 */
@Database(entities = [ToDoModel::class], version = 1, exportSchema = false)
abstract class AppDataBase : RoomDatabase() {
    abstract fun toDoDao(): ToDoDao
}