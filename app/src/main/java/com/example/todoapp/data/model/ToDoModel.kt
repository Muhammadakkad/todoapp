package com.example.todoapp.data.model

import androidx.room.Entity
import androidx.room.PrimaryKey
import com.example.todoapp.data.Constant
import java.io.Serializable

/**
 * Created by Muhammad AKKAD on 10/25/21.
 */
@Entity(tableName = Constant.TODO_TABLE)
data class ToDoModel(
    @PrimaryKey(autoGenerate = true)
    var id: Int,
    var title: String,
    var priority: Int,
    var description: String
) :Serializable{
}