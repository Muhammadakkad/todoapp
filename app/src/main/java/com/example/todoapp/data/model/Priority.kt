package com.example.todoapp.data.model

/**
 * Created by Muhammad AKKAD on 10/25/21.
 */
enum class Priority(priority: Int) {
    HIGH(0), MEDIUM(1), LOW(2)
}