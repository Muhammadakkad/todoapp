package com.example.todoapp.data.db

import androidx.room.*
import com.example.todoapp.data.model.ToDoModel

/**
 * Created by Muhammad AKKAD on 10/25/21.
 */
@Dao
interface ToDoDao {
    @Query("SELECT * FROM todo_table ORDER BY id DESC")
    suspend fun getAllToDos(): List<ToDoModel>

    @Insert(onConflict = OnConflictStrategy.IGNORE)
    suspend fun insertToDo(todo: ToDoModel)

    @Update(onConflict = OnConflictStrategy.REPLACE)
    suspend fun updateToDo(todo: ToDoModel)

    @Delete
    suspend fun deleteToDo(todo: ToDoModel)

    @Query("DELETE FROM todo_table")
    suspend fun deleteAll()


    @Query("SELECT * FROM todo_table WHERE title LIKE :query")
    fun search(query: String): List<ToDoModel>

    @Query("SELECT * FROM todo_table ORDER BY CASE WHEN priority LIKE 0 THEN 1 WHEN priority LIKE 1 THEN 2 WHEN priority LIKE 2 THEN 3 END")
    fun sortByHighPriority() : List<ToDoModel>

    @Query("SELECT * FROM todo_table ORDER BY CASE WHEN priority LIKE 2 THEN 1 WHEN priority LIKE 1 THEN 2 WHEN priority LIKE 0 THEN 3 END")
    fun sortByLowPriority() : List<ToDoModel>
}