package com.example.todoapp.data.db

import com.example.todoapp.data.model.ToDoModel
import javax.inject.Inject

/**
 * Created by Muhammad AKKAD on 10/25/21.
 */
class ToDoRepository @Inject constructor(val toDoDao: ToDoDao) {

    suspend fun getAllDAta(): List<ToDoModel> = toDoDao.getAllToDos()
    suspend fun sortByHigh(): List<ToDoModel> = toDoDao.sortByHighPriority()
    suspend fun sortByLow(): List<ToDoModel> = toDoDao.sortByLowPriority()

    suspend fun insertTodo(toDoModel: ToDoModel) {
        toDoDao.insertToDo(toDoModel)
    }

    suspend fun updateTodo(toDoModel: ToDoModel) {
        toDoDao.updateToDo(toDoModel)
    }

    suspend fun deleteTodo(toDoModel: ToDoModel) {
        toDoDao.deleteToDo(toDoModel)
    }

    suspend fun deleteAll() {
        toDoDao.deleteAll()
    }

    fun search(query: String): List<ToDoModel> {
        return toDoDao.search(query)
    }


}