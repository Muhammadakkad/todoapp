package com.example.todoapp.data

/**
 * Created by Muhammad AKKAD on 10/25/21.
 */
object Constant {
    const val TODO_TABLE = "todo_table"
    const val TODO_DATABASE = "todo_database"
}