package com.example.todoapp.ui.fragments

import android.app.AlertDialog
import android.os.Bundle
import android.view.*
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import com.example.todoapp.R
import com.example.todoapp.data.model.ToDoModel
import com.example.todoapp.databinding.FragmentUpdateBinding
import com.example.todoapp.ui.MainActivity
import com.example.todoapp.ui.viewmodels.ToDoViewModel
import com.google.android.material.snackbar.Snackbar
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class UpdateFragment : Fragment() {
    private var _binding: FragmentUpdateBinding? = null
    private val binding get() = _binding!!
    private val args by navArgs<UpdateFragmentArgs>()
    private val viewModel: ToDoViewModel by viewModels()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = FragmentUpdateBinding.inflate(inflater, container, false)
        val view = binding.root
        binding.args = args

        setHasOptionsMenu(true)
        return view
    }


    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        inflater.inflate(R.menu.update_fragment_menu, menu)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        activity?.let { MainActivity().hideKeyboard(it) }
        when (item.itemId) {
            R.id.menu_save -> updateToDo()
            R.id.menu_delete -> confirmDeletion()
        }
        return super.onOptionsItemSelected(item)
    }

    private fun deleteToDo() {
        viewModel.deleteToDo(getObject())
        navigateBack()
    }

    private fun confirmDeletion() {
        val builder = AlertDialog.Builder(requireContext())
        builder.setPositiveButton(getString(R.string.yes)) { _, _ ->
            deleteToDo()
            Snackbar.make(binding.root, getString(R.string.deleted), Snackbar.LENGTH_SHORT).show()
        }
        builder.setNegativeButton(getString(R.string.no)) { _, _ -> {} }
        builder.setTitle(getString(R.string.delete))
        builder.setMessage(getString(R.string.are_you_sure_to_delete))
        builder.show()
    }

    private fun updateToDo() {
        if (!verifyData()) {
            viewModel.updateToDo(getObject())
            Snackbar.make(binding.root, getString(R.string.updated), Snackbar.LENGTH_SHORT).show()
            navigateBack()
        } else
            Snackbar.make(binding.root, getString(R.string.check_inputs), Snackbar.LENGTH_SHORT)
                .show()
    }

    private fun getObject(): ToDoModel {
        val id = args.item.id
        val title = binding.titleEtFrgUpdate.text.toString()
        val description = binding.descriptionEtFrgUpdate.text.toString()
        val priority = binding.prioritySpFrgUpdate.selectedItemPosition
        return ToDoModel(id, title, priority, description)
    }

    private fun navigateBack() {
        findNavController().navigate(R.id.action_updateFragment_to_listFragment)
    }

    private fun verifyData(): Boolean {
        return binding.descriptionEtFrgUpdate.text.isNullOrBlank() || binding.titleEtFrgUpdate.text.isNullOrBlank()
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }
}