package com.example.todoapp.ui.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import com.example.todoapp.data.model.ToDoModel
import com.example.todoapp.databinding.TodoItemLayoutBinding

/**
 * Created by Muhammad AKKAD on 10/25/21.
 */
class TodoListAdapter(private val context: Context?) :
    RecyclerView.Adapter<TodoListAdapter.ViewHolder>() {
    var list = emptyList<ToDoModel>()

    inner class ViewHolder(itemView: TodoItemLayoutBinding) :
        RecyclerView.ViewHolder(itemView.root) {
        val _itemView = itemView

        fun bind(todoList: ToDoModel) {
            _itemView.todo = todoList
            _itemView.executePendingBindings()
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): TodoListAdapter.ViewHolder {
        val binding: TodoItemLayoutBinding = TodoItemLayoutBinding.inflate(
            LayoutInflater.from(parent.context),
            parent,
            false
        )
        return ViewHolder(binding)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val currentItem = list[position]
        holder.bind(currentItem)
    }

    override fun getItemCount(): Int {
        return list.size
    }

    fun setData(dataList: List<ToDoModel>) {
        val diffUtil = ListDiffUtil(list, dataList)
        val todoDiffResult = DiffUtil.calculateDiff(diffUtil)
        list = dataList
        todoDiffResult.dispatchUpdatesTo(this)
    }
}