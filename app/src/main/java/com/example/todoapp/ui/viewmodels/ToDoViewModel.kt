package com.example.todoapp.ui.viewmodels

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.todoapp.data.db.ToDoRepository
import com.example.todoapp.data.model.ToDoModel
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import javax.inject.Inject

/**
 * Created by Muhammad AKKAD on 10/25/21.
 */
@HiltViewModel
class ToDoViewModel @Inject constructor(private val toDoRepository: ToDoRepository) : ViewModel() {
    var toDoList = MutableLiveData<List<ToDoModel>>()
    var searchQueryResult = MutableLiveData<List<ToDoModel>>()

    fun getToDos() {
        viewModelScope.launch(Dispatchers.IO) {
            toDoList.postValue(toDoRepository.getAllDAta())
        }
    }

    fun sortByHigh() {
        viewModelScope.launch(Dispatchers.IO) {
            toDoList.postValue(toDoRepository.sortByHigh())
        }
    }

    fun sortByLow() {
        viewModelScope.launch(Dispatchers.IO) {
            toDoList.postValue(toDoRepository.sortByLow())
        }
    }

    fun inertToDo(toDoModel: ToDoModel) {
        viewModelScope.launch(Dispatchers.IO) {
            toDoRepository.insertTodo(toDoModel)
            getToDos()
        }
    }

    fun updateToDo(toDoModel: ToDoModel) {
        viewModelScope.launch(Dispatchers.IO) {
            toDoRepository.updateTodo(toDoModel)
        }
    }

    fun deleteToDo(toDoModel: ToDoModel) {
        viewModelScope.launch(Dispatchers.IO) {
            toDoRepository.deleteTodo(toDoModel)
            getToDos()
        }
    }

    fun deleteAll() {
        viewModelScope.launch(Dispatchers.IO) {
            toDoRepository.deleteAll()
            getToDos() // to refresh the list
        }
    }

    fun search(query: String) {
        viewModelScope.launch(Dispatchers.IO) {
            searchQueryResult.postValue(toDoRepository.search(query))
        }
    }
}