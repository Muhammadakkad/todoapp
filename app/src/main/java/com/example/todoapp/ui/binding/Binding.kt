package com.example.todoapp.ui.binding

import android.content.res.ColorStateList
import android.view.View
import android.widget.Spinner
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.databinding.BindingAdapter
import androidx.lifecycle.MutableLiveData
import androidx.navigation.findNavController
import com.example.todoapp.R
import com.example.todoapp.data.model.Priority
import com.example.todoapp.data.model.ToDoModel
import com.example.todoapp.ui.fragments.ListFragmentDirections
import com.google.android.material.floatingactionbutton.FloatingActionButton

/**
 * Created by Muhammad AKKAD on 11/2/21.
 */
class Binding {
    companion object {

        @BindingAdapter("android:navigateToAddFragment")
        @JvmStatic
        fun navigateToAddFragment(view: FloatingActionButton, navigate: Boolean) {
            view.setOnClickListener {
                view.findNavController().navigate(R.id.action_listFragment_to_addFragment)
            }
        }

        @BindingAdapter("android:emptyList")
        @JvmStatic
        fun emptyList(view: View, list: MutableLiveData<List<ToDoModel>>) {
            when (list.value?.isEmpty()) {
                true -> view.visibility = View.VISIBLE
                false -> view.visibility = View.GONE
            }
        }

        @BindingAdapter("android:getPriority")
        @JvmStatic
        fun getPriority(view: Spinner, priority: Int) {
            when (priority) {
                Priority.HIGH.ordinal -> view.setSelection(Priority.HIGH.ordinal)
                Priority.MEDIUM.ordinal -> view.setSelection(Priority.MEDIUM.ordinal)
                Priority.LOW.ordinal -> view.setSelection(Priority.LOW.ordinal)
            }
        }

        @BindingAdapter("android:getPriorityColor")
        @JvmStatic
        fun getPriorityColor(view: ConstraintLayout, priority: Int) {
            when (priority) {
                Priority.HIGH.ordinal -> view.backgroundTintList = ColorStateList.valueOf(
                    view.context?.getColor(
                        R.color.light_red
                    )!!
                )
                Priority.MEDIUM.ordinal -> view.backgroundTintList = ColorStateList.valueOf(
                    view.context?.getColor(
                        R.color.light_yellow
                    )!!
                )
                Priority.LOW.ordinal -> view.backgroundTintList = ColorStateList.valueOf(
                    view.context?.getColor(
                        R.color.light_green
                    )!!
                )
            }
        }

        @BindingAdapter("android:navigateToUpdateFragment")
        @JvmStatic
        fun navigateToUpdateFragment(view: ConstraintLayout, todo: ToDoModel) {
            view.setOnClickListener {
                view.findNavController()
                    .navigate(ListFragmentDirections.actionListFragmentToUpdateFragment(todo))
            }
        }

    }
}