package com.example.todoapp.ui.fragments

import android.app.AlertDialog
import android.os.Bundle
import android.view.*
import androidx.appcompat.widget.SearchView
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.recyclerview.widget.ItemTouchHelper
import androidx.recyclerview.widget.RecyclerView
import androidx.recyclerview.widget.StaggeredGridLayoutManager
import com.example.todoapp.R
import com.example.todoapp.data.model.ToDoModel
import com.example.todoapp.databinding.FragmentListBinding
import com.example.todoapp.ui.MainActivity
import com.example.todoapp.ui.adapter.SwipeToDelete
import com.example.todoapp.ui.adapter.TodoListAdapter
import com.example.todoapp.ui.viewmodels.ToDoViewModel
import com.google.android.material.snackbar.Snackbar
import dagger.hilt.android.AndroidEntryPoint
import jp.wasabeef.recyclerview.animators.LandingAnimator

@AndroidEntryPoint
class ListFragment : Fragment(), SearchView.OnQueryTextListener {

    private var _binding: FragmentListBinding? = null
    private val binding get() = _binding!!
    private lateinit var adapter: TodoListAdapter
    private val viewModel: ToDoViewModel by viewModels()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = FragmentListBinding.inflate(inflater, container, false)
        val view = binding.root
        binding.lifecycleOwner = this
        binding.viewmodel = viewModel
        setupTodoListRv()
        setupObservers()
        getData()
        setHasOptionsMenu(true)
        activity?.let { MainActivity().hideKeyboard(it) }
        return view
    }

    override fun onResume() {
        getData()
        super.onResume()
    }

    private fun getData() {
        viewModel.getToDos()
    }

    private fun setupObservers() {
        viewModel.toDoList.observe(viewLifecycleOwner, {
            adapter.setData(it)
        })

        viewModel.searchQueryResult.observe(viewLifecycleOwner, {
            it?.let {
                adapter.setData(it)
            }
        })
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        inflater.inflate(R.menu.list_fragment_menu, menu)
        val search = menu.findItem(R.id.menu_search)
        val searchView = search.actionView as? SearchView
        searchView?.isSubmitButtonEnabled = true
        searchView?.setOnQueryTextListener(this)
    }


    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            R.id.menu_delete_all -> confirmDeletion()
            R.id.menu_priority_high -> viewModel.sortByHigh()
            R.id.menu_priority_low -> viewModel.sortByLow()
            R.id.menu_priority_defult -> getData()
        }
        return super.onOptionsItemSelected(item)
    }

    private fun confirmDeletion() {
        val builder = AlertDialog.Builder(requireContext())
        builder.setPositiveButton(getString(R.string.yes)) { _, _ ->
            viewModel.deleteAll()
            Snackbar.make(binding.root, getString(R.string.deleted), Snackbar.LENGTH_SHORT).show()
        }
        builder.setNegativeButton(getString(R.string.no)) { _, _ -> {} }
        builder.setTitle(getString(R.string.delete))
        builder.setMessage(getString(R.string.are_you_sure_to_delete_all_notes))
        builder.show()
    }

    private fun setupTodoListRv() {
        val listRv = binding.recyclerView
        adapter = TodoListAdapter(context)
        listRv.adapter = adapter
        listRv.layoutManager = StaggeredGridLayoutManager(2, StaggeredGridLayoutManager.VERTICAL)
        listRv.itemAnimator = LandingAnimator().apply {
            addDuration = 300
        }
        swipeToDelete(listRv)
    }

    private fun swipeToDelete(recyclerView: RecyclerView) {
        val swipeToDelete = object : SwipeToDelete() {
            override fun onSwiped(viewHolder: RecyclerView.ViewHolder, direction: Int) {
                val item = adapter.list[viewHolder.adapterPosition]
                viewModel.deleteToDo(item)

                undoDeletion(viewHolder.itemView, item, viewHolder.adapterPosition)
            }
        }
        val itemTouchHelper = ItemTouchHelper(swipeToDelete)
        itemTouchHelper.attachToRecyclerView(recyclerView)
    }

    private fun undoDeletion(view: View, deletedItem: ToDoModel, position: Int) {
        val snackBar =
            Snackbar.make(binding.root, getString(R.string.deleted), Snackbar.LENGTH_SHORT)
        snackBar.setAction("Undo") {
            viewModel.inertToDo(deletedItem)
        }
        snackBar.show()
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }

    override fun onQueryTextSubmit(query: String?): Boolean {
        if (query != null) {
            search(query)
        }
        return true
    }

    private fun search(query: String) {
        var searchQuery = "%$query"
        viewModel.search(searchQuery)
    }

    override fun onQueryTextChange(newText: String?): Boolean {
        if (newText != null) {
            search(newText)
        }
        return true
    }
}