package com.example.todoapp.ui.fragments

import android.os.Bundle
import android.view.*
import androidx.core.widget.doAfterTextChanged
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import com.example.todoapp.R
import com.example.todoapp.data.model.ToDoModel
import com.example.todoapp.databinding.FragmentAddBinding
import com.example.todoapp.ui.MainActivity
import com.example.todoapp.ui.viewmodels.ToDoViewModel
import com.google.android.material.snackbar.Snackbar
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class AddFragment : Fragment() {

    private var _binding: FragmentAddBinding? = null
    private val binding get() = _binding!!
    private val viewModel: ToDoViewModel by viewModels()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = FragmentAddBinding.inflate(inflater, container, false)
        val view = binding.root
        setHasOptionsMenu(true)
        setupTextWatchers()
        return view
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        inflater.inflate(R.menu.add_fragment_menu, menu)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        activity?.let { MainActivity().hideKeyboard(it) }
        when (item.itemId) {
            R.id.menu_add -> insertToDo()
        }
        return super.onOptionsItemSelected(item)
    }

    private fun insertToDo() {
        val title = binding.titleEtFrgAdd.text.toString()
        val priority = binding.prioritySpFrgUpdate.selectedItemPosition
        val description = binding.descriptionEtFrgAdd.text.toString()
        if (!verifyData()) {
            viewModel.inertToDo(ToDoModel(0, title, priority, description))
            Snackbar.make(binding.root, getString(R.string.added), Snackbar.LENGTH_SHORT)
                .show()
            findNavController().navigate(R.id.action_addFragment_to_listFragment)
        } else {
            showError()
        }
    }

    private fun showError() {
        Snackbar.make(binding.root, getString(R.string.empty_fields), Snackbar.LENGTH_SHORT)
            .show()
        if (binding.titleEtFrgAdd.text.trim().isEmpty()) binding.titleEtFrgAdd.error =
            getString(R.string.required)
        if (binding.descriptionEtFrgAdd.text.trim()
                .isEmpty()
        ) binding.descriptionEtFrgAdd.error = getString(R.string.required)
    }

    private fun setupTextWatchers() {
        binding.titleEtFrgAdd.doAfterTextChanged { binding.titleEtFrgAdd.error = null }
        binding.descriptionEtFrgAdd.doAfterTextChanged { binding.descriptionEtFrgAdd.error = null }
    }

    private fun verifyData(): Boolean {
        return binding.descriptionEtFrgAdd.text.isNullOrBlank() || binding.titleEtFrgAdd.text.isNullOrBlank()
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }
}