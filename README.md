# todoapp
 
Fully Functional To-Do app using:

* Kotlin
* Navigation Component
* Safe args
* Transition Animation
* ROOM Database
* CRUD operations
* Data binding
* RecyclerView
* LiveData
* ViewModel
* Repository
* DiffUtil
* Linear, Grid, Staggered Layout in RecyclerView
* Clean Architecture
